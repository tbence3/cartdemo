<?php

use Illuminate\Database\Seeder;


class TestSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->publishers();
         $this->products();
         $this->discounts();

    }

    /**
     * Create Discounts
     */
    protected function discounts(){

        \App\Discount::create(
            [
                'id' => 101,
                'title' => '10%-os kedvezmény',
                'type' => 'percent',
                'value' => 10,
                'category' => 'products',
                'category_id' => 1006
            ]
        );

        \App\Discount::create(
            [
                'id' => 102,
                'title' => '500-os kedvezmény',
                'type' => 'fix',
                'value' => 500,
                'category' => 'products',
                'category_id' => 1002
            ]
        );

        \App\Discount::create(
            [
                'id' => 103,
                'title' => '2+1 csomag kedvezmény',
                'type' => 'combinate',
                'value' => 100,
                'category' => 'publishers',
                'category_id' => 1
            ]
        );
    }

    /**
     * Create Products
     */
    protected function products(){
        \App\Product::create(
            [
                'id' => 1001,
                'title' => 'Dreamweaver CS4',
                'author' => 'Janine Warner',
                'publisher_id' => 1,
                'price' => '3900',
            ]
        );

        \App\Product::create(
            [
                'id' => 1002,
                'title' => 'JavaScript kliens oldalon',
                'author' => 'Sikos László',
                'publisher_id' => 2,
                'price' => '2900',
            ]
        );

        \App\Product::create(
            [
                'id' => 1003,
                'title' => 'Java',
                'author' => 'Barry Burd',
                'publisher_id' => 1,
                'price' => '3700',
            ]
        );

        \App\Product::create(
            [
                'id' => 1004,
                'title' => 'C# 2008',
                'author' => 'Stephen Randy Davis',
                'publisher_id' => 1,
                'price' => '3700',
            ]
        );

        \App\Product::create(
            [
                'id' => 1005,
                'title' => 'Az Ajax alapjai',
                'author' => 'Joshua Eichorn',
                'publisher_id' => 1,
                'price' => '4500',
            ]
        );

        \App\Product::create(
            [
                'id' => 1006,
                'title' => 'Algoritmusok',
                'author' => 'Ivanyos Gábor, Rónyai Lajos, Szabó Réka',
                'publisher_id' => 3,
                'price' => '3600',
            ]
        );

    }


    /**
     * Create Publishers
     */
    protected function publishers(){
        \App\Publisher::create(
            [
                'name' => 'PANEM'
            ]
        );
        \App\Publisher::create(
            [
                'name' => 'BBS-INFO'
            ]
        );
        \App\Publisher::create(
            [
                'name' => 'TYPOTEX'
            ]
        );
    }
}
