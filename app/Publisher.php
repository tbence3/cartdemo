<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    public $timestamps = true;

    protected $table = 'publishers';

    protected $fillable = ['name'];
}
