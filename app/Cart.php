<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $table = 'carts';
    protected $dates = ['deleted_at'];
    protected $fillable = ['session_id',];

    public function getProducts()  {
        return $this->belongsToMany('App\ProductStructure', 'cart_products', 'cart_id', 'product_id');

        return $this->hasManyThrough('App\Product', '\App\CartProduct', 'cart_id', 'id','id');

        return $this->hasManyThrough('App\Product', '\App\CartProduct', 'cart_id', 'id','product_id');

        return $this->hasMany('\App\CartProduct','cart_id','id')->with('product');
    }

    public static function getUserCart(){
        return self::where('session_id','=',session()->getId())->first();
    }

}
