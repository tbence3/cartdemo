<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;
use App\Cart;

class CartController extends Controller
{
    protected $cartModel;

    function __construct(){

        $cart = \App\Cart::getUserCart();

        if(!is_null($cart)){
            $cart->delete();

            $this->create();
        }
        else {
            $this->create();
        }

    }

    /**
     * Create Cart
     * @return static
     */
    public function create()
    {
        $this->cartModel = Cart::create(['session_id'=>session()->getId()]);
        return $this->cartModel;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function addProduct(Product $product){

        $pivot = \App\CartProduct::create([
                'cart_id'=>$this->cartModel->id,
                'product_id'=>$product->id,
            ]);
        return true;
    }

    public static function getCart(){
        return \App\CartStructure::getUserCart();
    }
}
