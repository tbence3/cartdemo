<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class APIv1Controller extends Controller
{
    /**
     * Send back Products.
     *  Options: orderBy Title or Price
     * @param null $by
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts($by = null)
    {
        $products = \App\ProductStructure::all();

        if(!is_null($by)){
            switch($by){
                case'name':
                    $products = \App\ProductStructure::orderBy('title')->get();
                    break;
                case'price':
                    $products = \App\ProductStructure::orderBy('price')->get();
                    break;
            }

        }


        return response()->json($products);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCart(Request $request)
    {

        $cartCont = new CartController();

        $input = $request->all(); //get input datas

        foreach($input['products'] as $product){
            $product = \App\Product::find($product['id']); //Check product is exist?

            if(!is_null($product)){
                $cartCont->addProduct($product);
            }

        }

        $cart = \App\CartStructure::getUserCart();

        return response()->json($cart);

    }

    /**
     * Send back user's cart.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCart()
    {

        $cart = CartController::getCart();

        return response()->json($cart);

    }



}
