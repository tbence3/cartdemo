<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('teszt_feladat');
});

Route::group(['prefix' => 'api/v1'], function () {

    Route::get('product', 'APIv1Controller@getProducts');
    Route::get('product/sort/{by}', 'APIv1Controller@getProducts');
    Route::post('cart', 'APIv1Controller@createCart');
    Route::get('cart', 'APIv1Controller@getCart');
});