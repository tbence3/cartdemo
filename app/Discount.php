<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    public $timestamps = true;

    protected $table = 'discounts';

    protected $fillable = ['id','title','type','value','category','category_id'];
}
