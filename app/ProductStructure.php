<?php
/**
 * Created by PhpStorm.
 * User: Bence Turi
 */

namespace App;


class ProductStructure extends Product
{
    protected $appends = ['discount','discount_type' ,'publisher'];


    /**
     * Set product's publisher name
     * @return mixed
     */
    public function getPublisherAttribute()
    {
        return $this->getPublisher->name;
    }

    /**
     * Set product's discount attribute
     * @return mixed|string
     */
    public function getDiscountAttribute()
    {
        return $this->calculateDiscount();
    }

    /**
     * Set product's discount tyoe attribute
     * @return string
     */
    public function getDiscountTypeAttribute()
    {
        return $this->getDiscountType();
    }

    /**
     * Calculate discount of product
     * @return mixed|string
     */
    protected function calculateDiscount(){

        $discount = $this->getDiscount();

        switch($discount->type){
            case'percent':
                return $this->price * ((100-$discount->value)/100);
                break;
            case'fix':
                return $this->price - $discount->value;
                break;
            case'combinate':
                return "2+1 Akció";
                break;
            default:
                return '';
                break;
        }

    }


    /**
     * @return string
     */
    protected function getDiscountType(){

        $discount = $this->getDiscount();

        switch($discount->type){
            case'percent':
                return 'fix';
                break;
            case'fix':
                return 'fix';
                break;
            case'combinate':
                return "2+1";
                break;
            default:
                return 'none';
                break;
        }

    }


    /**
     * ProductStructure constructor.
     */
    function __construct(){
        parent::__construct();

        return;
    }
}


