<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = true;

    protected $table = 'products';

    protected $fillable = ['id','publisher_id','author','price','title'];


    public function getPublisher()  {
        return $this->hasOne('\App\Publisher','id','publisher_id');
    }

    public function getProductDiscounts(){

        return \App\Discount::where('category','=','products')->where('category_id','=',$this->id)->first();
    }

    public function getCategoryDiscounts(){

        return \App\Discount::where('category','=','publishers')->where('category_id','=',$this->publisher_id)->first();
    }

    public function getDiscount(){
        $prod = $this->getProductDiscounts();
        $cat = $this->getCategoryDiscounts();

        if(!is_null($prod)){
            return $this->getProductDiscounts();
        }
        elseif(!is_null($cat)) {
            return $this->getCategoryDiscounts();
        }
        else {
            return 'nincs kedvezmény';
        }


    }


}
