<?php
/**
 * Created by PhpStorm.
 * User: Zulu
 * Date: 2016.09.09.
 * Time: 15:04
 */

namespace App;


class CartStructure extends Cart
{
    protected $appends = ['subTotal','discount','total','products'];

    protected $subtotal;
    protected $total;
    protected $discount;

    public function getProductsAttribute()
    {
        return $this->getProducts()->get();
    }

    public function getSubtotalAttribute()
    {
        return (int)$this->calcTotal();
    }

    public function getDiscountAttribute()
    {
        return $this->calcDiscount($this->getProducts()->get());
    }

    public function getTotalAttribute()
    {
        return $this->calcTotal() - $this->calcDiscount($this->getProducts()->get());
    }

    function __construct(){
        parent::__construct();

        //print_r($this->getProductDiscounts());
        return;
    }

    protected function calcDiscount($products){
        $total = 0;
        $discountProd = [];

        foreach($products as $product){
            if($product->discount_type == 'fix'){
                $total += ($product->price - $product->discount);
            }
            else {
                $discountProd[] = $product->toArray();
            }
        }
        $total += $this->calcGroupDiscount($discountProd);

        return $total;

    }


    protected function calcGroupDiscount($products){
        $freePcs = (sizeof($products)/3)>> 0;
        $total = 0;


        //Rendezees ar szerint
        usort($products, function($a, $b) {
            return $a['price'] - $b['price'];
        });


        for($i=0;$i<sizeof($products);$i++){
            if(($i< $freePcs)){
                //ingyenes teteleket nem szamolom bele
                $total += $products[$i]['price'];
            }
        }

        return $total;
    }

    protected function calcTotal(){
        return $this->getProducts()->sum('price');
    }

}
