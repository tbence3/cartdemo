var products = [];
var cartBox = [];
var cart = [];

function addToCartBox(productID){
    $("#cartBoxEmpty").hide('slow');
    $("#cartBoxFooter").show('slow');


    var product = {};

    for(var i=0;i<products.length;i++){
        if(products[i].id == productID){
            product = products[i];
            break;
        }
    }

    cartBox[cartBox.length] = product;

    var compiled = make('cartLine',product);

    $('#carBoxtItems').append(compiled);

    $('#cardBoxTotal').html(calculateTotal(cartBox,'price')+' Ft');
}

function calculateTotal(objectArray,index){
    var total = 0;
    var hasGroup = false;
    var group = [];

    for(var i=0;i<objectArray.length;i++){


        if(objectArray[i].discount_type == "fix") {
            total += objectArray[i].discount;
        }
        else {
            hasGroup = true;
            group[group.length] = objectArray[i];
        }

    }
    if(hasGroup){
        total += calcGroup2p1Discount(group);
    }

    return total;
}

function calcGroup2p1Discount(arrayObject){
    var freePcs = (arrayObject.length/3)>> 0;
    var total = 0;

    arrayObject.sort(SortByPrice); //Sorting array

    for(var i=0;i<arrayObject.length;i++){

        if(!(i<freePcs)){
            //ingyenes teteleket nem szamolom bele
            total += arrayObject[i].price;
        }
    }

    return total;
}

function SortByPrice(a, b){
    var aPrice = a.price;
    var bPrice = b.price;
    return ((aPrice < bPrice) ? -1 : ((aPrice > bPrice) ? 1 : 0));
}


function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}


function make(template,data,elementId = null){

    var template = $("#"+template).html();
    var compiled = _.template(template);
    var tmp = '';

    if ($.isArray(data)){

        for(var i=0;i<data.length;i++){
            tmp += compiled(data[i]);
        }
    }
    else{
        tmp = compiled(data);
    }

    if(elementId != null){
        $(elementId).html(tmp);
    }
    else {
        return tmp;
    }
}

function loadProducts(sort=''){
    $.ajax({

        url: '/api/v1/product'+sort,
        type: 'GET',
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $("#message").html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Internal Error...</div>');
            console.log('Error');
            console.log(textStatus+' '+errorThrown);
        },
        success: function(data){
            products = data;
            make('tableDateLine',data,'#tableLinePlace');
            //console.log(products);
        }
    });
}

function setCart(){
    $.ajax({

        url: '/api/v1/cart',
        type: 'POST',
        data: {products:cartBox},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $("#message").html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Internal Error...</div>');
            console.log('Error');
            console.log(textStatus+' '+errorThrown);
        },
        success: function(data){
            make('cartLine',data.products,'#carItems');
            setupCart(data);
            console.log(data);
        }
    });
}


function getCart(){
    $.ajax({

        url: '/api/v1/cart',
        type: 'GET',
        data: {products:cartBox},
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $("#message").html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Internal Error...</div>');
            console.log('Error');
            console.log(textStatus+' '+errorThrown);
        },
        success: function(data){
            make('cartLine',data.products,'#carItems');
            setupCart(data);
        }
    });
}

function setupCart(data){
    $('#cartFooter').show('slow');
    $('#cartSubTotal').html(data.subTotal+' Ft');
    $('#cardDiscont').html(data.discount+' Ft');
    $('#cardTotal').html(data.total+' Ft');

}
