<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="<?php echo csrf_token() ?>"/>

    <title>Big Fish teszt feladat</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="/css/starter-template.css" rel="stylesheet">

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Teszt feladat</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Főoldal</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">

    <div class="starter-template">
        <p class="lead">Jó vásárlást a Demó webshopban!</p>
    </div>
    <div id="message"></div>

    <div class=" ">
        <table class="table table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th><a href="javascript:void(0)" onclick="loadProducts('/sort/name');">Cím</a></th>
                <th>Szerző</th>
                <th>Kiadó</th>
                <th><a href="javascript:void(0)" onclick="loadProducts('/sort/price');">Ára</a></th>
                <th>Kedvezményes ár</th>
                <th>Akciók</th>
            </tr>
            </thead>
            <tbody id="tableLinePlace">
            <tr>
                <td><i class="fa fa-refresh" aria-hidden="true"></i></td>
                <td><i class="fa fa-refresh" aria-hidden="true"></i></td>
                <td><i class="fa fa-refresh" aria-hidden="true"></i></td>
                <td><i class="fa fa-refresh" aria-hidden="true"></i></td>
                <td><i class="fa fa-refresh" aria-hidden="true"></i></td>
                <td><i class="fa fa-refresh" aria-hidden="true"></i></td>
                <td><i class="fa fa-refresh" aria-hidden="true"></i></td>
            </tr>
            </tbody>
        </table>
    </div>



<hr>

<div class="col-sm-4">
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Dobozod</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group" id="carBoxtItems">
                <li class="list-group-item" id="cartBoxEmpty">Üres a dobozod</li>
            </ul>
            <div id="cartBoxFooter" style="display: none">
                <div>Összesen: <span id="cardBoxTotal"></span></div>
                <button type="button" class="btn btn-sm btn-success" onclick="setCart();">Kosárba helyezés</button>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-4">
</div>
<div class="col-sm-4">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Kosarad</h3>
        </div>
        <div class="panel-body">
            <ul class="list-group" id="carItems">
                <li class="list-group-item" id="cartEmpty">Üres a kosarad</li>
            </ul>
            <div id="cartFooter" style="display: none">
                <div>Részösszeg: <span id="cartSubTotal"></span></div>
                <div>Kedvezmény mértéke: <span id="cardDiscont"></span></div>
                <div>Végösszeg: <span id="cardTotal"></span></div>
            </div>
        </div>
    </div>
</div>

</div><!-- /.container -->

<script type="text/template" id="tableDateLine">
    <tr>
        <td><%= id %></td>
        <td><%= title %></td>
        <td><%= author %></td>
        <td><%= publisher %></td>
        <td><%= price %> Ft</td>
        <td><%= discount  %> </td>
        <td><button type="button" class="btn btn-sm btn-success" onclick="addToCartBox(<%= id %>);">Dobozba vele!</button></td>
    </tr>
</script>
<script type="text/template" id="cartLine">
    <li class="list-group-item">1db <%= title %> </li>
</script>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/js/underscore-min.js"></script>

<script type="text/javascript" src="/js/cart.js"></script>

<script>
    $(document).ready(function(){

        loadProducts();
        getCart();

    });

    $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
        var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

        if (token) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
        }
    });

</script>

</body>
</html>
